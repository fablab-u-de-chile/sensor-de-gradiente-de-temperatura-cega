/*
 * Este codigo conecta un dispositivo LilyGo-SIM800L-IP5306-20190610
 * con Adafruit IO usando una red Wifi y protocolo de datos MQTT
 * ref https://github.com/Xinyuan-LilyGO/LilyGo-T-Call-SIM800/blob/master/schematic/LilyGo-SIM800L-IP5306-20190610.pdf
 *
*/

/* next to "lilygo_wifi_mqtt_aio.ino" in file "secrets.h" you'll need to define: */
/*
  #define WLAN_SSID       // your wifi ssid
  #define WLAN_PASS       // your wifi password

  #define AIO_SERVER      "io.adafruit.com"
  #define AIO_SERVERPORT  1883
  #define AIO_USERNAME    // your adafruit io username
  #define AIO_KEY         // your adafruit io key
*/

#include "secrets.h"
#include <RTClib.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <SparkFun_TMP117.h>

#define PUB_DATA_ONLINE true
#define LOG_SD_CARD true
#define DEBUG_SERIAL true

// total number of sensors connected
#define numTMP 8

/* FOR WIFI CONNECTION */
#include <WiFi.h>

// Create an ESP32 WiFiClient object to connect to the MQTT server.
WiFiClient client;

#include <Adafruit_MQTT.h>
#include <Adafruit_MQTT_Client.h>

/* ADAFRUIT IO PUBLISHERS */
Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);
Adafruit_MQTT_Publish t1 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/tmp117.t1");
Adafruit_MQTT_Publish t2 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/tmp117.t2");
Adafruit_MQTT_Publish t3 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/tmp117.t3");
Adafruit_MQTT_Publish t4 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/tmp117.t4");
Adafruit_MQTT_Publish t5 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/tmp117.t5");
Adafruit_MQTT_Publish t6 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/tmp117.t6");
Adafruit_MQTT_Publish t7 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/tmp117.t7");
Adafruit_MQTT_Publish t8 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/tmp117.t8");
Adafruit_MQTT_Publish VBAT = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/tmp117.vbat");
Adafruit_MQTT_Publish *pubs[numTMP] = {&t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8};

#define uS_TO_S_FACTOR 1000000ULL     /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP 60//7200
/*  Time ESP32 will go to sleep (in seconds)
    3600 seconds = 1 hour
    600  seconds = 10 minutes
    60   seconds = 1 minute
*/

// HW shield pin definitions
#define HW_POWER_SWITCH     32
#define DEBUG_LED           13
#define BATT_MONITOR_PIN    34 //(ADC1_CH6)

// redefine los pines del uart1
// los que vienen por defecto en el esp32 (9,10) no se pueden utilizar para uart externo
#define OL_RX 2
#define OL_TX 15
// pin para reiniciar el OpenLog conectado al pin GRN
#define OL_RESET_PIN  12

// I2C 1st port pins
#define I2C_SDA              21
#define I2C_SCL              22
// I2C 2nd port pins
#define I2C_SDA_2            25
#define I2C_SCL_2            26

// TMP117 sensors

TwoWire I2C_P1 = TwoWire(0);
TMP117 tmp1; // p1-1 dir 0x48
TMP117 tmp3; // p1-2 dir 0x49
TMP117 tmp5; // p1-3 dir 0x4A
TMP117 tmp7; // p1-4 dir 0x4B

#define TMP1_ADDR 0x48
#define TMP3_ADDR 0x49
#define TMP5_ADDR 0x4A
#define TMP7_ADDR 0x4B

// I2C P2
TwoWire I2C_P2 = TwoWire(1);
TMP117 tmp2; // p2-1 dir 0x48
TMP117 tmp4; // p2-2 dir 0x49
TMP117 tmp6; // p2-3 dir 0x4A
TMP117 tmp8; // p2-4 dir 0x4B

#define TMP2_ADDR 0x48
#define TMP4_ADDR 0x49
#define TMP6_ADDR 0x4A
#define TMP8_ADDR 0x4B

TMP117 *tmps[numTMP] = {&tmp1, &tmp2, &tmp3, &tmp4, &tmp5, &tmp6, &tmp7, &tmp8};
int sensors[numTMP] = {1, 2, 3, 4, 5, 6, 7, 8};
int addrs[numTMP] = {TMP1_ADDR, TMP2_ADDR, TMP3_ADDR, TMP4_ADDR, TMP5_ADDR, TMP6_ADDR, TMP7_ADDR, TMP8_ADDR};

// for temperature values
float temp[numTMP];

#define OL_BAUDRATE 9600 // open log serial comm baudrate
char buff[50];

int batteryLevel = 0;
RTC_DS3231 RTC; // define the Real Time Clock object, connect only to default I2C port (21,22)

#define SerialMon Serial
#define OpenLog Serial2

#define NUM_SAMPLES         10
char mesg[20] = "";

#define IP5306_ADDR          0x75
#define IP5306_REG_SYS_CTL0  0x00
bool setPowerBoostKeepOn(int en) {
  I2C_P1.beginTransmission(IP5306_ADDR);
  I2C_P1.write(IP5306_REG_SYS_CTL0);
  if (en) {
    I2C_P1.write(0x37); // Set bit1: 1 enable 0 disable boost keep on
  } else {
    I2C_P1.write(0x35); // 0x37 is default reg value
  }
  return I2C_P1.endTransmission() == 0;
}

void setup() {
  gpio_hold_dis(GPIO_NUM_32);

  
#ifdef DEBUG_SERIAL
  // Set serial monitor debugging window baud rate to 115200
  SerialMon.begin(115200);

  SerialMon.println("START PROGRAM");
  SerialMon.println(String("[INFO] PUB_ONLINE = ") + (PUB_DATA_ONLINE ? "TRUE" : "FALSE"));
  SerialMon.println(String("[INFO] LOG_SD_CARD = ") + (LOG_SD_CARD ? "TRUE" : "FALSE"));

#endif

  OpenLog.begin(OL_BAUDRATE,SERIAL_8N1,OL_RX,OL_TX); //Open software serial port at 9600bps
  I2C_P1.begin(I2C_SDA, I2C_SCL, 400000);
  I2C_P2.begin(I2C_SDA_2, I2C_SCL_2, 400000);

  pinMode(HW_POWER_SWITCH, OUTPUT);
  digitalWrite(HW_POWER_SWITCH, LOW);

  pinMode(DEBUG_LED, OUTPUT);
  blinkLed(DEBUG_LED, 1);

  // Keep power when running from battery
  bool isOk = setPowerBoostKeepOn(1);

#ifdef DEBUG_SERIAL
  SerialMon.println(String("IP5306 KeepOn ") + (isOk ? "OK" : "FAIL"));
#endif

  setupHW();
  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);

  /* MAIN PROGRAM FUNCTIONALITY */
  // get actual date
  DateTime now = RTC.now();
  unsigned long utime = now.unixtime();

  // get battery voltage
  batteryLevel = getBatteryVoltage();
  //Serial.println("Vbat: " + String(batteryLevel));

  getSensorData();
  
  logData(utime, batteryLevel);
 
#if PUB_DATA_ONLINE == true
  WiFi_connect();
  if (MQTT_connect() == true)
    pubData();  // publica datos de los sensores y voltaje de la bateria

#endif

  //digitalWrite(DEBUG_LED, 1);
  //delay(3000);
  //digitalWrite(DEBUG_LED, 0);
  turnOffHW();
  delay(1000);


#ifdef DEBUG_SERIAL
  Serial.println("going to deep sleep mode... bye");
#endif

  gpio_hold_en(GPIO_NUM_32);
  gpio_deep_sleep_hold_en();
  esp_deep_sleep_start();
}

void loop() {

}

void WiFi_connect() {

#ifdef DEBUG_SERIAL
  Serial.println("Connecting to WiFi...");
#endif
 
  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    
#ifdef DEBUG_SERIAL
    Serial.print(".");
#endif
  }
  //Serial.println();

#ifdef DEBUG_SERIAL
  Serial.print("..WiFi connected");  
  Serial.print("IP address: "); Serial.println(WiFi.localIP());
#endif

}

bool MQTT_connect()
{
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return true;
  }

#ifdef DEBUG_SERIAL
  Serial.print("Connecting to MQTT... ");
#endif

  uint8_t retries = 5;

  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
    Serial.println(mqtt.connectErrorString(ret));
 
#ifdef DEBUG_SERIAL
    Serial.print("Retrying MQTT connection in 5 seconds... retries left: ");
    Serial.print(retries);
    Serial.print("... ");
#endif    
    mqtt.disconnect();

    delay(5000); // wait 5 seconds

    retries--;
    if (retries == 0) {
      // basically die and wait for WDT to reset me
      // while (1);

#ifdef DEBUG_SERIAL      
      Serial.println(mqtt.connectErrorString(ret));
#endif
      return false;
    }
  }

#ifdef DEBUG_SERIAL  
  Serial.println("MQTT Connected!");
#endif  
  return true;
}

void pubData() {
  for (int i = 0; i < numTMP; i++) {
    sprintf(mesg, "temp sensor t%d: ", sensors[i]);
    Serial.print(mesg);
    Serial.print(temp[i], 2);
    Serial.print(" ... ");
    if (!pubs[i]->publish(temp[i])) {
      Serial.println(F("pub Failed"));
    } else {
      Serial.println(F("pub OK!"));
    }
    delay(200);
  }
  delay(500);
  // publica voltaje de la bateria
  VBAT.publish(batteryLevel);
  delay(500);
}

void getSensorData() {
  // geta data from sensors
  for (int i = 0; i < numTMP; i++) {
    if (tmps[i]->dataReady() == true) {
      temp[i] = tmps[i]->readTempC();
      sprintf(mesg, "temp sensor t%d: ", sensors[i]);
      Serial.print(mesg);
      Serial.println(temp[i], 2);

    }
  }
}

void logData(unsigned long utime, int battery) {

#if LOG_SD_CARD == true
  OpenLog.print(utime);
  OpenLog.print(';'); 
  OpenLog.print(battery);
  for(int i = 0; i<numTMP; i++){
    OpenLog.print(';');
    OpenLog.print(temp[i]);
  }
#endif

#ifdef DEBUG_SERIAL
  Serial.print("Logged: ");
  Serial.print(utime);
  Serial.print(";");
  Serial.print(battery);
  for (int i = 0; i < numTMP; i++) {
    Serial.print(';');
    Serial.print(temp[i]);
  }
  Serial.println(';');
#endif

}

void turnOnHW() {
  /*
     configura todos los pines digitales en LOW
     apaga el transistor que alimenta el Hardware externo al esp32
  */
 
#ifdef DEBUG_SERIAL
  Serial.println("Turn ON external HW");
#endif  
  digitalWrite(HW_POWER_SWITCH, HIGH);

}

void turnOffHW() {
  /*
     configura todos los pines digitales en LOW
     apaga el transistor que alimenta el Hardware externo al esp32
  */
#ifdef DEBUG_SERIAL
  Serial.println("Turn OFF external HW");
#endif  
  digitalWrite(HW_POWER_SWITCH, LOW);

}

int getBatteryVoltage() {
  int batReading = 0;
  int num_samples = NUM_SAMPLES;
  // take analog samples
  for (int i = 0; i < num_samples; i++) {
    batReading += analogRead(BATT_MONITOR_PIN);
    delay(1);
  }
  float vBat = (batReading / num_samples) * 3.35 / 4096.0 * 2;
  int vBatI = vBat * 1000;
  //Serial.println(vBat);
  //Serial.println(vBatI);
  return vBatI;
}

void setupHW() {
  turnOnHW();
  delay(500);

  if (!RTC.begin()) {
    SerialMon.println("RTC failed");
  }

  setupOpenLog();

  // tmp117 initialization
  for (int i = 0; i < numTMP; i++) {
    if (i % 2 == 0) {
      if (tmps[i]->begin(addrs[i], I2C_P1) == true) {
        sprintf(mesg, "begin sensor: %d, at addr: 0x%X", sensors[i], addrs[i]);
        //sprintf(mesg, "begin sensor %d", i + 1);
        SerialMon.println(mesg);
      }
      else {
        sprintf(mesg, "failed sensor: %d, at addr: 0x%X", sensors[i], addrs[i]);
        SerialMon.println(mesg);
        //while (1); // Runs forever
      }
    }
    else {
      if (tmps[i]->begin(addrs[i], I2C_P2) == true) {
        sprintf(mesg, "begin sensor: %d, at addr: 0x%X", sensors[i], addrs[i]);
        //sprintf(mesg, "begin sensor %d", i + 1);
        SerialMon.println(mesg);
      }
      else {
        sprintf(mesg, "failed sensor: %d, at addr: 0x%X", sensors[i], addrs[i]);
        SerialMon.println(mesg);
        //while (1); // Runs forever
      }
    }
  }
}

void setupOpenLog() {
  //Reset OpenLog
  digitalWrite(OL_RESET_PIN, LOW);
  delay(100);
  digitalWrite(OL_RESET_PIN, HIGH);
}

void blinkLed(int LED, int T)
{
  for (int i = 0; i < T; i++)
  {
    digitalWrite(LED, HIGH);
    delay(50);
    digitalWrite(LED, LOW);
    delay(50);
  }
}
